import java.util.HashMap;

public class IsomorphicStrings {

    public boolean isIsomorphic (String firstString, String secondString) {

            if (firstString == null || firstString.length() <= 1 )
                return true;

        HashMap<Character, Character> map = new HashMap<>();

        for (int i = 0; i < firstString.length(); i++) {
            char a = firstString.charAt(i);
            char b = secondString.charAt(i);
            if (map.containsKey(a)) {
                if (!map.get(a).equals(b)) {
                    return false;
                }
            }else {
                if (!map.containsValue(b))
                    map.put(a, b);
                else
                    return false;
            }
        }
        
        return true;
    }
}
