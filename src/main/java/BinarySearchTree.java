public class BinarySearchTree {

    public class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
        }
    }

    private TreeNode searchBST(TreeNode root, int number) {
        if (root == null || root.val == number) {
            return root;
        }
        if (root.val > number) {
            return searchBST(root.left, number);
        }
        return searchBST(root.right, number);
    }
}
