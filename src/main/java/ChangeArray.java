import java.lang.reflect.Array;
import java.util.Arrays;

public class ChangeArray {

    public static int[][] flipAndInvertImage(int[][] array) {
        for (int i = 0; i < array.length; i++) {
            int[] innerArray = array[i];
            for (int j = 0; j < innerArray.length; j++){
                if (array[i][j] == 0) {
                    array[i][j] = 1;
                } else {
                    array[i][j] = 0;
                }
            }
        }
        return array;
    }

    public static void main(String[] a) {
        int[][] arr = {{1, 1, 0}, {1, 0, 1}, {0, 0, 0}};
        flipAndInvertImage(arr);
        System.out.println(Arrays.deepToString(arr));
    }
}
