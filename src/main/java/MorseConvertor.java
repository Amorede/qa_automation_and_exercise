import java.util.*;

public class MorseConvertor {

    private String[] morseCode = {".-", "-...", "-.-.", "-..", ".", "..-.", "--.", "....", "..", ".---",
            "-.-", ".-..", "--", "-.", "---", ".--.", "--.-", ".-.", "...", "-", "..-",
            "...-", ".--", "-..-", "-.--", "--.."};
    private static Map<Character, String> morseVocab;

    MorseConvertor(String locale) {
        switch (locale) {
            case "en":
                morseVocab = createEngMap();
        }
    }

    public String wordToMorze(String word) {
        StringBuilder toReturn = new StringBuilder();
        for (char ch : word.toCharArray()) {
            toReturn.append(morseVocab.get(ch));

        }
        return toReturn.toString();
    }



    public String morseToWord(String morseCode) {
        String[] morseChar = morseCode.split(" ");
        StringBuilder sb = new StringBuilder();
        for(String l : morseChar) {
            sb.append(getKeysByValue(l));
        }

        return  sb.toString();
    }


    private String getKeysByValue( String word) {
        for (Map.Entry<Character, String> entry : morseVocab.entrySet()) {
            if (Objects.equals(word, entry.getValue())) {
               return entry.getKey().toString();
            }
        }
        return "";
    }



    private Map<Character, String> createEngMap() {
        char c = 'a';
        Map<Character, String> map = new HashMap<>();
        for (int i = 0; i < morseCode.length; i++, c++) {
            String k = morseCode[i];
            map.put(c, k);
        }
        return map;
    }

}
class MapUtils{
//    public String getKeysByValue( String word) {
//        for (Map.Entry<Character, String> entry : morseVocab.entrySet()) {
//            if (Objects.equals(word, entry.getValue())) {
//                return entry.getKey().toString();
//            }
//        }
//        return "";
//    }

}


