import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class ReverseVowels {

    public String reverseVowels(String phrase) {

        Set<Character> vowel = new HashSet<>();
        vowel.add('a');
        vowel.add('e');
        vowel.add('i');
        vowel.add('o');
        vowel.add('u');
        vowel.add('A');
        vowel.add('E');
        vowel.add('I');
        vowel.add('O');
        vowel.add('U');

        char[] phraseChars = phrase.toCharArray();
        String last = new String();
        for (int i = 0, j = phrase.length() -1 ; i <= j ;) {
            if (!vowel.contains(phraseChars[j])){
                j--;
            }
           else if (!vowel.contains(phraseChars[i])){
                i++;
            }
            if (vowel.contains(phraseChars[i]) && vowel.contains(phraseChars[j])) {
                char temp = phraseChars[j];
                phraseChars[j] = phraseChars[i];
                phraseChars[i] = temp;
                i++;
                j--;
            }
            last = Arrays.toString(phraseChars);
        }
        return last;
    }

    public static void main(String[] args) {
        ReverseVowels reverseVowels = new ReverseVowels();
        String s = "oao ueu";
        String a = "OAO UEU";
        System.out.println(reverseVowels.reverseVowels(s));
        System.out.println(reverseVowels.reverseVowels(a));
    }
}
