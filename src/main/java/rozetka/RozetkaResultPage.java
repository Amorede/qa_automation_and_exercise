package rozetka;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public class RozetkaResultPage {

    private WebDriver webDriver;

    private WebDriverWait webDriverWait;

    @FindBy(xpath = "//div[@class='g-i-tile g-i-tile-catalog']//li[@class='g-tools-i']")
    private WebElement addToWishlistFirstElement;

    @FindBy(xpath = "//a[@class='hub-i-link hub-i-wishlist-link-count sprite-side whitelink']")
    private WebElement wishlistButton;


    @FindBy(xpath = "//input[@name='login']")
    private WebElement inputFakeEmail;

    @FindBy(xpath = "//button[@class='btn-link-i']")
    private WebElement buttonSaveEmail;

    @FindBy(xpath = "//input[@name='fio']")
    private WebElement inputFakeName;

    @FindBy(xpath = "//button[@name='confirm_signup_submit']")
    private WebElement buttonSaveName;


    @FindBy(xpath = "//a[@href='https://rozetka.com.ua/ua/apple_iphone_xs_max_256gb_mt762/p58289805/']")
    private WebElement actualPhone;

    public  WebElement getActualPhone() {
        return actualPhone;
    }


    public RozetkaResultPage (final WebDriver driver) {
        this.webDriver = driver;
        PageFactory.initElements(driver, this);
    }

    public void clickAddToWishList(){
        addToWishlistFirstElement.click();
    }

    public void inputFakeEmail(String text) {
        inputFakeEmail.sendKeys(text);
    }

    public void clickSaveFakeEmail(){
        buttonSaveEmail.click();
    }

   public void inputFakeName(String text) {
       inputFakeName.sendKeys(text);
    }

    public void clickSaveName() {
        buttonSaveName.click();
    }

    public RozetkaWishlist clickOnWishlist(){
        wishlistButton.click();
        return new RozetkaWishlist(webDriver);
    }
}
