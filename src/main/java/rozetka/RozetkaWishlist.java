package rozetka;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class RozetkaWishlist {

    private WebDriver webDriver;

    @FindBy(xpath = "//a[@href='https://rozetka.com.ua/ua/apple_iphone_xs_max_256gb_mt762/p58289805/']")
    private WebElement actualPhoneAtWishlist;

    public  WebElement getIphoneAtWishlist() {
        return actualPhoneAtWishlist;
    }

    public RozetkaWishlist (final WebDriver driver) {
        this.webDriver = driver;
        PageFactory.initElements(driver, this);
    }
}
