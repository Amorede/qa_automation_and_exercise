package rozetka;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class RozetkaMainPage {

    private WebDriver webDriver;

    @FindBy(xpath = "//input[@class='rz-header-search-input-text passive']")
    private WebElement inputSearchString;


    public RozetkaMainPage (final WebDriver driver) {
        this.webDriver = driver;
        PageFactory.initElements(driver, this);
    }

    public RozetkaResultPage inputProductName(final String text) {
        inputSearchString.sendKeys(text, Keys.ENTER);
        return new RozetkaResultPage (webDriver);
    }
}
