public class LicenseKey {

    public static String licenseKeyFormatting(String input, int k){

        StringBuilder modInput = new StringBuilder(input.replaceAll("-", "").toUpperCase());

        for (int j = modInput.length()-k; j > 0  ; j = j-k) {
            modInput.insert(j, "-");
        }

        return modInput.toString();
    }

    public static void main(String [] args){
        LicenseKey licenseKey = new LicenseKey();
        System.out.println(licenseKeyFormatting("2-5g-3-J", 2));
    }
}
