public class ReverseVowelsOld {

    public String reverseVowels(String phrase) {
        StringBuilder stringVovels = new StringBuilder();
        for (int i = 0; i < phrase.length(); i++) {

            if(((phrase.toCharArray()[i] == 'a') || (phrase.toCharArray()[i] == 'e') || (phrase.toCharArray()[i] == 'i') || (phrase.toCharArray()[i] == 'o') || (phrase.toCharArray()[i] == 'u')) ||
                    ((phrase.toCharArray()[i] == 'A') || (phrase.toCharArray()[i] == 'E') || (phrase.toCharArray()[i] == 'I') || (phrase.toCharArray()[i] == 'O') || (phrase.toCharArray()[i] == 'U'))){
                stringVovels.append(phrase.toCharArray()[i]);
            }
        }

        String reversedVowels = stringVovels.reverse().toString();
        StringBuilder result = new StringBuilder();
//        String result = "";
        int j = 0;
        for (int i = 0; i < phrase.length(); i++) {
            if (((phrase.toCharArray()[i] == 'a') || (phrase.toCharArray()[i] == 'e') || (phrase.toCharArray()[i] == 'i') || (phrase.toCharArray()[i] == 'o') || (phrase.toCharArray()[i] == 'u')) ||
                    ((phrase.toCharArray()[i] == 'A') || (phrase.toCharArray()[i] == 'E') || (phrase.toCharArray()[i] == 'I') || (phrase.toCharArray()[i] == 'O') || (phrase.toCharArray()[i] == 'U'))) {
                result.append(phrase.toCharArray()[i] = reversedVowels.charAt(j));
                j++;
            } else {
                result.append(phrase.toCharArray()[i]);
            }
        }
        return result.toString();
    }
}
