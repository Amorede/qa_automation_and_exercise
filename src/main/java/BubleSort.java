
public class BubleSort {
    // приймає массив чисел і сортірує його від меньшого до більшого на місці

    public static void sort(int[] args) {
        if (args == null) {
            return;
        }

        for (int i = 0; i < args.length - 1;) {
            int x = args[i];
            int y = args[i+1];
            if (x > y) {
                args[i+1] = x;
                args[i] = y;
                i = 0;
            } else {
                i++;
            }
        }
    }
}