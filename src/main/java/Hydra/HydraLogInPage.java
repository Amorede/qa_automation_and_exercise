package Hydra;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HydraLogInPage {

    private WebDriver webDriver;
    private WebDriverWait webDriverWait;
    private Actions actions;


    public HydraLogInPage(final WebDriver driver) {
        this.webDriver = driver;
        actions = new Actions(driver);
        webDriverWait = new WebDriverWait(driver, 30);
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//input[@class='input-text__field ng-untouched ng-pristine ng-invalid' and (@formcontrolname='email')]")
    private WebElement emailInputField;

    @FindBy(xpath = "//input[@formcontrolname='password']")
    private WebElement passwordInputField;

    @FindBy(xpath = "//button[@type='submit']")
    private WebElement logInButton;

    @FindBy(xpath = "//button[@class='input-text__button']")
    private WebElement showPassword;

    @FindBy(xpath = "//input[@formcontrolname='password' and (@type=  'password')]")
    private WebElement passwordHiden;

    @FindBy(xpath = "//input[@formcontrolname='password' and (@type=  'text')]")
    private WebElement passwordShown;

    @FindBy(xpath = "//button[@class='login-form__link']")
    private WebElement forgotPasswordButton;

    @FindBy(xpath = "//p[@class='input-text__error-text']")
    private WebElement emailOrPasswordIsNotCorrect;

    @FindBy(xpath = "//h1[@class='login-form__title']")
    private WebElement titleForgotYourPassword;

    public boolean titleForgotYourPasswordAShown(final String forgotYourPassword) {
        webDriverWait.until(ExpectedConditions.visibilityOf(titleForgotYourPassword));
        return forgotYourPassword.equals(titleForgotYourPassword.getText());
    }

    public void inputEmail(final String emailInput) {
        emailInputField.sendKeys(emailInput);
    }

    public void inputPassword(final String passwordInput) {
        passwordInputField.sendKeys(passwordInput);
    }

    public void clickOnShowPassword(){
        showPassword.click();
        if (passwordShown.isDisplayed()){
            showPassword.click();
        }
    }

    public void clickOnForgotPassword(){
        forgotPasswordButton.click();
    }

    public void clickLogInButton(){
        logInButton.click();
    }

    public HydraDashboard clickLogInButtonToOpenDashboard(){
        logInButton.click();
        return new HydraDashboard (webDriver);
    }

    public boolean errorEmailOrPasswordIsNotCorrectShown(){
        return webDriverWait.until(ExpectedConditions.invisibilityOf(emailOrPasswordIsNotCorrect));
    }

}
