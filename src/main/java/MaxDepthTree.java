import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class MaxDepthTree {

    class Node {

        public int val;
        public List<Node> children;

        public Node() {
        }

        public Node(int _val, List<Node> _children) {
            val = _val;
            children = _children;
        }
    }

    public int maxDepth(Node root) {
        if (root == null) {
            return 0;
        }
        if (root.children.isEmpty()) {
            return 1;
        } else {
            List<Integer> depth = new LinkedList<>();
            for (Node item : root.children) {
                depth.add(maxDepth(item));
            }
            return Collections.max(depth) + 1;
        }
    }
}
