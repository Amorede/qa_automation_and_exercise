package Boocking;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;

public class BoockingResultPage {


    private WebDriver webDriver;
    private WebDriverWait webDriverWait;

    public BoockingResultPage(final WebDriver driver) {
        this.webDriver = driver;
        webDriverWait = new WebDriverWait(driver, 30);
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//a[@class=' jq_tooltip  district_link visited_link ']")
    private List<WebElement> destination;


    public List<String> getListOfDestination () {
        final List<String> listOfdestinations = new ArrayList<>();
        for (WebElement cell : destination) {
                listOfdestinations.add(cell.getText());

        } return listOfdestinations;
    }
}
