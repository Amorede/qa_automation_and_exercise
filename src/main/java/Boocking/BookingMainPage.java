package Boocking;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class BookingMainPage {

    private WebDriver webDriver;
    private WebDriverWait webDriverWait;
    private Actions actions;


    public BookingMainPage(final WebDriver driver) {
        this.webDriver = driver;
        actions = new Actions(driver);
        webDriverWait = new WebDriverWait(driver, 30);
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//input[@id='ss']")
    private WebElement whereAreYouGoing;

    @FindBy(xpath = "//div[@class='fly-dropdown fly-dropdown--onload-shower fly-dropdown_bottom fly-dropdown_arrow-right']//div[@class='bicon bicon-aclose header-signin-prompt__close']")
    private WebElement popUp;

    @FindBy(xpath = "//li[@data-label='Boston, Massachusetts, USA']")
    private WebElement bostonMassahusetts;

    @FindBy(xpath = "//button[@aria-label='Open calendar']")
    private WebElement checkIn;

    @FindBy(xpath = "//label[@id='xp__guests__toggle']")
    private WebElement guests;

    @FindBy(xpath = "//div[@class='xp__guests__inputs focussable']")
    private WebElement guestsFocusable;

    @FindBy(xpath = "//div[@class='sb-group__field']//div[@class='bui-stepper']//button[@class='bui-button bui-button--secondary bui-stepper__add-button']")
    private WebElement addAdults;

    @FindBy(xpath = "//select[@id='group_adults']")
    private WebElement addSelectAdults;

    @FindBy(xpath = "//div[@class='bui-calendar__month']")
    private List<WebElement> month;

    @FindBy(xpath = "//div[@class='bui-calendar__control bui-calendar__control--next']")
    private WebElement nextMonth;


    @FindBy(xpath = "//button[@class='sb-searchbox__button  ']")
    private WebElement searchButton;

    private static final String CURRENT_MONTH_AND_YEAR = "(//div[@class='bui-calendar__month'])[1]";

    private static final String CURRENT_DAYS = "//div[@class='bui-calendar__wrapper'][1]//td[@data-bui-ref='calendar-date' and contains(@data-date, '%s')]";

    public void setAddSelectAdults(Integer adultsIndex, String adultCount) {
        int length = webDriver.findElements(By.xpath("//div[@class='sb-group__field']//div[@class='bui-stepper']//button[@class='bui-button bui-button--secondary bui-stepper__add-button']")).size();
        if (length == 1) {
            clickPlus(adultCount);
        } else {
            selectAddAdults(adultsIndex);
        }
    }

    public void selectAddAdults(final Integer adultsIndex) {
        final Select adultQtySelect = new Select(addSelectAdults);
        adultQtySelect.selectByIndex(adultsIndex);
    }

    public void clickPlus(final String adultsIndex) {
        while (!adultsIndex.equals(webDriver.findElement(By.xpath("//input[@id='group_adults']")).getAttribute("value"))) {
            addAdults.click();
        }
    }

    public void clickOnCalendar() {
        checkIn.click();
    }

    public void clickOnClosePopUp() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(popUp)).click();
    }

    public void clickOnGuests() {
        guests.click();
    }

    public void clickOnBoston() {
        bostonMassahusetts.click();
    }

    public void typeWhereToGo(final String WhereToGo) {
        whereAreYouGoing.sendKeys(WhereToGo);
    }

    public void selectMonth(final String monthAndYear) {
        while (!webDriver.findElement(By.xpath(CURRENT_MONTH_AND_YEAR)).getText().equalsIgnoreCase(monthAndYear)) {
            webDriverWait.until(ExpectedConditions.elementToBeClickable(nextMonth)).click();
        }
    }

    public void selectStartDay(final String startDay) {
        final String fullDayLocator = String.format(CURRENT_DAYS, startDay);
        final WebElement requiredStartDay = webDriver.findElement(By.xpath(fullDayLocator));
        requiredStartDay.click();
    }

    public void selectEndDay(final String endDay) {
        final String fullDayLocator = String.format(CURRENT_DAYS, endDay);
        final WebElement requiredDay = webDriver.findElement(By.xpath(fullDayLocator));
        requiredDay.click();
    }

    public BoockingResultPage clickSearchButton() {
        searchButton.click();
        return new BoockingResultPage(webDriver);
    }

}
