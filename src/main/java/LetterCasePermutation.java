import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;


public class LetterCasePermutation {
//
//    public static List<String> letterCasePermutation(String word) {
//        List<String> variants = new ArrayList<>();
//        char[] wordCharArray = word.toCharArray();
//        StringBuilder sb =  new StringBuilder();
//        for (int i = 0; i < wordCharArray.length; i++) {
//
//            if (Character.isLowerCase(wordCharArray[i])){
//                char n = Character.toUpperCase(wordCharArray[i]);
//                .split
//                sb.append(Character.toUpperCase(wordCharArray[i]));
////                sb.replace(wordCharArray[i], wordCharArray[n], word);
//                variants.add(String.valueOf(sb));
////                return letterCasePermutation(word);
////                break;
//            }
//            System.out.println(variants);
//        }
//        return variants;
//    }
    public static void main(String[] a){

        Solution solution = new Solution();
        List<String> list = solution.letterCasePermutation("aqwe");
        System.out.println(list);

//       LetterCasePermutation.letterCasePermutation("aqwe");
    }

   static class Solution {
        public List<String> letterCasePermutation(String S) {
            List<String> ans = new ArrayList<>();
            compute(ans, S.toCharArray(),0);
            return ans;
        }

        public void compute(List<String> ans, char[] chars, int index)
        {
            if(index==chars.length)
                ans.add(new String(chars));
            else
            {
                if(Character.isLetter(chars[index]))
                {
                    chars[index]=Character.toLowerCase(chars[index]);
                    compute(ans,chars,index+1);
                    chars[index]=Character.toUpperCase(chars[index]);
                }
                compute(ans,chars,index+1);
            }
        }
    }
}
