
import java.util.HashMap;
import java.util.Map;

public class RepeatingNumbersHashMap {

    public static void main(String[] args) {


        Map<Integer, Integer> states = new HashMap<>();

        int[] array = {1, 1, 2, 3, 3, 3, 25, 65, 45, 1, 1, 1, 1, 1};

        for (int number : array) {
            if (states.containsKey(number)) {
                int counter = states.get(number);
                counter++;
                states.put(number, counter);
            } else {
                states.put(number, 1);
            }
        }
        System.out.println(states.toString());
    }
}
