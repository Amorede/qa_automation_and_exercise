package Fly;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class FlyMainPage {

    private WebDriver webDriver;

    private Actions actions;

    public FlyMainPage(final WebDriver driver) {
        this.webDriver = driver;
        actions = new Actions(driver);
        PageFactory.initElements(driver, this);
    }


    @FindBy(xpath = "//input[@id='onewayButton']")
    private WebElement oneWayBtn;

    @FindBy(xpath = "//label[@class='stnCheckbox PCMN-checkbox']//input[@value='20006416']")
    private WebElement cheapOairCheckbox;

    @FindBy(xpath = "//label[@class='stnCheckbox PCMN-checkbox']//input[@checked]")
    private WebElement jetCostCheckbox;

    @FindBy(xpath = "//input[@id='searchinput-from']")
    private WebElement inputFrom;

    @FindBy(xpath = "//li[@id='JBC']")
    private WebElement autoFlightFrom;

    @FindBy(xpath = "//li[@id='BOB']")
    private WebElement autoFlightTo;

    @FindBy(xpath = "//input[@id='searchinput-to']")
    private WebElement inputTo;

    @FindBy(xpath = "//div[@class='calendar_selection_new_cal']")
    private WebElement departDate;

    @FindBy(xpath = "//a[@title='Next']")
    private WebElement nextBtn;

    @FindBy(xpath = "//span[@class='ui-datepicker-month']")
    private WebElement month;

    @FindBy(xpath = "//button[@id='search-btn']")
    private WebElement searchBtn;

    @FindBy(xpath = "//div[@id='ui-datepicker-div']//td")
    private List<WebElement> columns;


    public void clickOneWayButton() {
        oneWayBtn.click();
    }

    public void clickcheapOairCheckbox() {
        cheapOairCheckbox.click();
    }

    public void clickJetCostCheckbox() {
        jetCostCheckbox.click();
    }

    public void clickInputFrom() {
        inputFrom.click();
    }

    public void clickAutoInputFromBOB() {
        autoFlightFrom.click();
    }

    public void clickInputTo() {
        inputTo.click();
    }

    public void clickAutoInputToJBC() {
        autoFlightTo.click();
    }

    public void typeFrom(final String text) {
        inputFrom.sendKeys(text);
    }

    public void typeTo(final String text) {
        inputTo.sendKeys(text);
    }

    public void clickCalendar() {
        departDate.click();
    }

    public FlyResultPage clickSearchBtn() {
        searchBtn.click();
        return new FlyResultPage(webDriver);
    }



    public void clickNextUntillMonthAndDate(final String monthInput, String date) {
        while (!month.getText().equals(monthInput)){
            nextBtn.click();
        }
        for (WebElement cell : columns) {

            if (cell.getText().equals(date)) {
                cell.click();
                break;
            }
        }
    }
}
