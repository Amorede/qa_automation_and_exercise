package Fly;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class FlyResultPage {

    private WebDriver webDriver;

    private WebDriverWait webDriverWait;

    private static final Long ELEMENT_WAIT_TIME_OUT = 50L;

    public FlyResultPage(final WebDriver driver) {
        this.webDriver = driver;
        webDriverWait = new WebDriverWait(webDriver, ELEMENT_WAIT_TIME_OUT);
        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
        PageFactory.initElements(driver, this);
    }
    @FindBy(xpath = "//div[@class='timing_belt']//*[contains(text(),'Depart - Tue 12 Feb 2019 - ')]")
    private List<WebElement> elementListOfActualDates;


    @FindBy(xpath = "//tr[@id='lowfaresTitleTR']")
    private WebElement tabWait;


    @FindBy(xpath = "//a[@class='btn btn-primary btn-lg btn-block search_btn_big toggleShowHideDetails']")
    private List<WebElement> expandButtons;



    public List<String> getFilterDates() {
        final List<String> filtredItemsTextList = new ArrayList<String>();
        for (WebElement element : elementListOfActualDates) {
            filtredItemsTextList.add(element.getText());

        }
        return filtredItemsTextList;
    }

    public void clickOnEveryButton() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(tabWait));
        for (WebElement cell : expandButtons) {
            cell.click();
        }
    }


    public boolean checkAssert(final String expectedDate) {

        for (WebElement cell : elementListOfActualDates) {
            if (cell.getText().equals(expectedDate)) {
                return true;
            }
        }
        return false;
    }
}
