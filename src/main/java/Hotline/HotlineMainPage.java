package Hotline;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class HotlineMainPage {



    @FindBy(xpath = "//li[@class='level-1 auto']//a")
    private WebElement autoBtn;


    private WebDriver webDriver;

    public HotlineMainPage (final WebDriver driver) {
        this.webDriver = driver;
        PageFactory.initElements(driver, this);
    }

    public HotlineAutoPage clickAutoBtn() {
        autoBtn.click();
        return new HotlineAutoPage(webDriver);
    }
}
