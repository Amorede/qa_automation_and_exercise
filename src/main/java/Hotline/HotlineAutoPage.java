package Hotline;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.ArrayList;
import java.util.List;

public class HotlineAutoPage {


    @FindBy(xpath = "//ul[@class='bricks cell-list']//span[@class='bricks-box']//span[@class='h4']")
    private List<WebElement> titles;

    private WebDriver webDriver;

    public HotlineAutoPage (final WebDriver driver) {
        this.webDriver = driver;
        PageFactory.initElements(driver, this);
    }


    public List<String> getTitlesOfTheCategoriesOnTheAutoPage() {
        final List<String> titlesOfTheCategories = new ArrayList<>();
        for (WebElement element: titles) {
            titlesOfTheCategories.add(element.getText());
        }
        return titlesOfTheCategories;
    }

}
