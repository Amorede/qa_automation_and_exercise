import java.util.ArrayList;
import java.util.Optional;
import java.util.function.Predicate;

class Contact {

    String name;
    String phoneNumber;

    @Override
    public String toString() {
        return name + "; " + phoneNumber;
    }
}

class ContactsManager {

    private ArrayList<Contact> myFriends = new ArrayList<>();

   Contact  createContact(String name, String phoneNumber){

        Contact newFriend =  new Contact();
        newFriend.name = name;
        newFriend.phoneNumber = phoneNumber;

       return newFriend;
   }

    void addContact(Contact contact) {
        myFriends.add(contact);
    }

    Optional<Contact> searchContact(String searchName) {
       return myFriends.stream().filter(
               contact -> searchName.equals(contact.name)
       ).findFirst();
    }

    public static void main(String [] args){

        ContactsManager contactsManager = new ContactsManager();

        Contact jessy = contactsManager.createContact("Jessy",  "0555-666");
        contactsManager.addContact(jessy);
        Contact missy = contactsManager.createContact("Missy",  "0555-666");
        contactsManager.addContact(missy);
        System.out.println(contactsManager.myFriends.toString());
        String searchCondition = "Missy";
        Optional<Contact> contact = contactsManager.searchContact(searchCondition);
        if(contact.isPresent()){
            System.out.println("contact - " + contact.get());
        } else {
            System.out.println("Contanct with " + searchCondition + " not found");
        }

    }
}