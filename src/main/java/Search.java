public class Search {


    int foundIndex(String[] arg, String element){
        for (int i = 0 ; i < arg.length; i++) {
            if (element.equals(arg[i])) {
                return i;
            }
        }
        return -1;
    }
}