import java.util.List;

public class DeleteNode {


    public class ListNode {
        int val;
        ListNode next;
        ListNode(int x) { val = x; }
    }


    public void deleteNodeSolution(ListNode node){
        node.next = node.next.next;
        node.val = node.next.val;
    }
}
