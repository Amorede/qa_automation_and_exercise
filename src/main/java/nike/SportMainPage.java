package nike;

import Core.SearchResultPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SportMainPage {

    @FindBy(xpath = "//input[@class='header-search__input ui-autocomplete-input']")
    private WebElement searchInput;

    @FindBy(xpath = "//input[@class='rfk_name' and contains(text(), 'Nike Swoosh Headband')]")
    private WebElement productButton;

    @FindBy(xpath = "//div[@class='header-account__trigger']")
    private WebElement signInButton;

    @FindBy(xpath = "//div[@class='header-account__sign-in']")
    private WebElement signInText;

    @FindBy(xpath = "//input[@class='signin-form__email']")
    private WebElement emailField;

    @FindBy(xpath = "//input[@class='signin-form__password']")
    private WebElement passwordField;


    @FindBy(xpath = "//input[@class='signin-form__submit button']")
    private WebElement signInSubmitButton;

    @FindBy(xpath = "(//li[@class='rfk_product'])[1]//a")
    private WebElement firstPickedJustForYouItem;

    public ProductDetailsPage selectFirstElementFromThePickedJustForYouSection() {
        firstPickedJustForYouItem.click();
        return new ProductDetailsPage(webDriver);
    }

    private WebDriver webDriver;

    private WebDriverWait webDriverWait;

    private static final Long ELEMENT_WAIT_TIME_OUT = 30L;
    private static final Long ELEMENT_WAIT_TIME_OUT_FAST = 10L;

    public SportMainPage(final WebDriver webDriver){
        this.webDriver = webDriver;
        webDriverWait = new WebDriverWait(webDriver, ELEMENT_WAIT_TIME_OUT);
        PageFactory.initElements(webDriver, this);
    }

    public void typeSearchText (final String text) {
        searchInput.sendKeys(text);
        webDriverWait.until(ExpectedConditions.elementToBeClickable(productButton));
    }

    public void clickSearchInput () {
        webDriverWait = new WebDriverWait(webDriver, ELEMENT_WAIT_TIME_OUT_FAST);
        webDriverWait.until(ExpectedConditions.elementToBeClickable(searchInput));
        searchInput.click();
    }

    public void clickSignInButton () {
        signInButton.click();
    }


    public ProductPage clickProductButton () {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(productButton));
        productButton.click();
        return new ProductPage(webDriver);
    }

    public void typeEmail (final String text) {
        emailField.sendKeys(text);
        webDriverWait.until(ExpectedConditions.elementToBeClickable(productButton));
    }

    public void clickPasswordField () {
        passwordField.click();
    }

    public void typePassword (final String text) {
        passwordField.sendKeys(text);
        webDriverWait.until(ExpectedConditions.elementToBeClickable(productButton));
    }

    public void clickSignInSubmitButton () {
        signInSubmitButton.click();
    }

    public void clickOut() {
        signInText.click();
    }

}
