import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RomanToInteger {

    public static void main(String[] sdr) {
        RomanConverter romanConverter = RomanConverter.getInstance();
        String convertValue = "CXCXCX";
        List<Integer> result = romanConverter.romanToInteger(convertValue);
        int sum = romanConverter.sum(convertValue);

        System.out.println(convertValue + " - " + result);
        System.out.println(convertValue + " sum" + sum);

    }

}

class RomanConverter {

    Map<String, Integer> romanMap = createRomanMap();
//    Map<Integer, String> reverseRomanMap = createRevertRomanMap();

    private static RomanConverter INSTANCE = null;


    private RomanConverter() {
    }

    static RomanConverter getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new RomanConverter();
        }
        return INSTANCE;
    }

    private Map<String, Integer> createRomanMap() {
        Map<String, Integer> romanMap = new HashMap<>();
        romanMap.put("I", 1);
        romanMap.put("V", 5);
        romanMap.put("X", 10);
        romanMap.put("L", 50);
        romanMap.put("C", 100);
        romanMap.put("D", 500);
        romanMap.put("M", 1000);
        romanMap.put("IV", 4);
        romanMap.put("IX", 9);
        romanMap.put("XL", 40);
        romanMap.put("XC", 90);
        romanMap.put("CD", 400);
        romanMap.put("CM", 900);
        return romanMap;
    }

//    private Map<Integer, String> createRevertRomanMap() {
//        Map<Integer, String> romanMap = new HashMap<>();
//        romanMap.put("I", 1);
//        romanMap.put("V", 5);
//        romanMap.put("X", 10);
//        romanMap.put("L", 50);
//        romanMap.put("C", 100);
//        romanMap.put("D", 500);
//        romanMap.put("M", 1000);
//        romanMap.put("IV", 4);
//        romanMap.put("IX", 9);
//        romanMap.put("XL", 40);
//        romanMap.put("XC", 90);
//        romanMap.put("CD", 400);
//        romanMap.put("CM", 900);
//        return romanMap;
//    }

    public List<Integer> romanToInteger(String roman) {
        List<Integer> finalArray = new ArrayList<>();
        for (int i = 0; i < roman.length(); i++) {
            String val = String.valueOf(roman.charAt(i));
            int next = i + 1;
            if (i + 1 < roman.length()) {
                String nextVal = String.valueOf(roman.charAt(i + 1));
                if (isRomanFrom2Letters(val, nextVal)) {
                    finalArray.add(romanMap.get(val + nextVal));
                    i++;
                    continue;
                }
            }

            finalArray.add(romanMap.get(val));

        }
        return finalArray;
    }

    private Map<String, String[]> get2LettersMap() {
        Map<String, String[]> hashMap = new HashMap<>();
        String[] values = {"V", "X"};
        hashMap.put("I", values);
        String[] values1 = {"L", "C"};
        hashMap.put("X", values1);
        String[] values2 = {"D", "M"};
        hashMap.put("C", values2);
        return hashMap;
    }

    private boolean isRomanFrom2Letters(String first, String second) {
        String[] letters = get2LettersMap().get(first);
        return letters != null && (letters[0].equals(second) || letters[1].equals(second));
    }

    int sum(String romanNumber) {
        return romanToInteger(romanNumber).stream()
                .mapToInt(Integer::intValue)
                .sum();
    }

}
