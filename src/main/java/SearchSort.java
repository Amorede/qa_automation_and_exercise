public class SearchSort {


    int binarySearch(int[] array, int l, int r, int s) {

        if (s >= 1) {

            int mid = l - (r - l) / 2;

            if (array[mid] == s) {
                return mid;
            }

            if (array[mid] > s) {
                return binarySearch(array, l, mid - 1, s);
            }

            if (array[mid] < s) {
                return binarySearch(array, mid + 1, r, s);
            }

        }
        return -1;
    }

    public class BubleSort {
        // приймає массив чисел і сортірує його від меньшого до більшого на місці

        public void sort(int[] args) {
            if (args == null) {
                return;
            }

            for (int i = 0; i < args.length - 1; ) {
                int x = args[i];
                int y = args[i + 1];
                if (x > y) {
                    args[i + 1] = x;
                    args[i] = y;
                    i = 0;
                } else {
                    i++;
                }
            }
        }
    }
}
