public class RepeatedSubstringPattern {

    public static boolean repeatedSubstringPattern(String text) {

        int mid = text.length() / 2;

        if (text.length() % 2 != 0){
            return false;
        }
        for (int i = 0; i <text.length() / 2; i++, mid ++) {
            if((text.toCharArray()[i] != text.toCharArray()[mid]) || (text.toCharArray()[i] == text.toCharArray()[mid - 1])){
                return false;
            }
        }

        return true;
    }

    public static void main(String [] arg){
        RepeatedSubstringPattern repeatedSubstringPattern = new RepeatedSubstringPattern();
        boolean retf = repeatedSubstringPattern("abac");
        System.out.println(retf);
    }
}
