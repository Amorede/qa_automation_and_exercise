package sportcehck;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;

public class SportCheckMainPage {

    @FindBy(xpath = "//input[@id='lst-ib']")
    private WebElement searchInput;

    @FindBy(xpath = "//input[@name='btnK']")
    private WebElement searchButton;

    @FindBy(xpath = "//span[@class='menu-toggle__text']")
    private WebElement shopCategories;

    @FindBy(xpath = "//ul[@class='page-nav__list page-nav__list_short main-menu']//li[@class='page-nav__item']")
    private List<WebElement> filtredItems;

    private WebDriver webDriver;

    private WebDriverWait webDriverWait;

    private static final Long ELEMENT_WAIT_TIME_OUT = 30L;


    public SportCheckMainPage (final WebDriver driver) {
        this.webDriver = driver;
        webDriverWait = new WebDriverWait(webDriver, ELEMENT_WAIT_TIME_OUT);
        PageFactory.initElements(webDriver, this);
    }

    public void expandFilterBox() {
        shopCategories.click();
    }

    public List<String> getFilterItemsTextList() {
        final List<String> filtredItemsTextList = new ArrayList<String>();
        for (final WebElement element : filtredItems) {
            filtredItemsTextList.add(element.getText());

        }

        return filtredItemsTextList;
    }



    public SearchResultPage typeSearchText (final String text) {
        searchInput.sendKeys(text, Keys.ENTER);
        return new SearchResultPage(webDriver);
    }

    public SearchResultPage clickSearchButton () {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(searchButton));
        searchButton.click();
        return new SearchResultPage(webDriver);
    }

}
