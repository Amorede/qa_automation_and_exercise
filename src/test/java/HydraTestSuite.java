import Hydra.HydraDashboard;
import Hydra.HydraLogInPage;
import common.BaseTest;
import org.junit.Assert;
import org.junit.Test;

public class HydraTestSuite extends BaseTest {

    @Test
    public void checkPasswordVisibilityButton() {
        getDriver().get("http://www.hydra-01.idap.pro");
        final HydraLogInPage hydraLogInPage = new HydraLogInPage(getDriver());
        hydraLogInPage.inputPassword("123456789");
        hydraLogInPage.clickOnShowPassword();
    }

    @Test
    public void checkErrorIncorrectEmailOrPassword() {
        getDriver().get("http://www.hydra-01.idap.pro");
        final HydraLogInPage hydraLogInPage = new HydraLogInPage(getDriver());
        hydraLogInPage.inputEmail("dmytro.zaiarnyi@idapgroup.com");
        hydraLogInPage.inputPassword("123");
        hydraLogInPage.clickLogInButton();
        Assert.assertTrue(hydraLogInPage.errorEmailOrPasswordIsNotCorrectShown());
    }

    @Test
    public void checkLogin() {
        getDriver().get("http://www.hydra-01.idap.pro");
        final HydraLogInPage hydraLogInPage = new HydraLogInPage(getDriver());
        hydraLogInPage.inputEmail("dmytro.zaiarnyi@idapgroup.com");
        hydraLogInPage.inputPassword("123456789");
        final HydraDashboard hydraDashboard = hydraLogInPage.clickLogInButtonToOpenDashboard();
    }

    @Test
    public void checkForgotPassword() {
        getDriver().get("http://www.hydra-01.idap.pro");
        final HydraLogInPage hydraLogInPage = new HydraLogInPage(getDriver());
        hydraLogInPage.clickOnForgotPassword();
        hydraLogInPage.inputEmail("dmytro.zaiarnyi@idapgroup.com");
        Assert.assertTrue(hydraLogInPage.titleForgotYourPasswordAShown("Forgot your Password"));
        hydraLogInPage.clickOnForgotPassword();
    }

//    @Test
//    public void checkForgotPassword() {
//        getDriver().get("http://www.hydra-01.idap.pro");
//        final HydraLogInPage hydraLogInPage = new HydraLogInPage(getDriver());
//
//    }
}
