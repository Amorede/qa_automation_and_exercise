import common.BaseTest;
import nike.ProductDetailsPage;
import nike.SportMainPage;
import org.junit.Assert;
import org.junit.Test;
import sportcehck.SportCheckMainPage;

import java.util.List;

public class SportcheckTestSuite extends BaseTest {

    @Test
    public void checkFilterItems() {
        final String[] expectedFilterItems = {
                "Deals & Features",
                "Men",
                "Women",
                "Kids",
                "Shoes & Footwear",
                "Gear",
                "Electronics",
                "Jerseys & Fan Wear",
                "Sneaker Launches",
                "Shop by Brand",
                "Chek advice"
        };
        getDriver().get("https://www.sportchek.ca/");
        final SportCheckMainPage sportCheckMainPage = new SportCheckMainPage(getDriver());
        sportCheckMainPage.expandFilterBox();
        final List<String> actualFilterItems = sportCheckMainPage.getFilterItemsTextList();
        Assert.assertArrayEquals("Thethe are incorrect",
                expectedFilterItems, actualFilterItems.toArray());
    }

    @Test
    public void clickOnTheFirstItemInTheList() {
        getDriver().get("https://www.sportchek.ca/");
        final SportMainPage page = new SportMainPage(getDriver());

        page.clickSearchInput();
        page.typeSearchText("nike");
        page.clickSearchInput();
        page.typeSearchText("nike");
        page.clickProductButton();
    }


    @Test
    public void checkEmailRegistrationErrors() {
        getDriver().get("https://www.sportchek.ca/");
        final SportMainPage page = new SportMainPage(getDriver());

        page.clickSignInButton();
        page.typePassword("666");
        page.clickOut();
        page.typeEmail("AmoA_Mila@gmail.com");
        page.clickPasswordField();
        page.clickSignInSubmitButton();
    }


    @Test
    public void checkMiniCartProductInfo() {
        getDriver().get("https://www.sportchek.ca/");
        final SportMainPage sportMainPage  = new SportMainPage(getDriver());
        final ProductDetailsPage productDetailsPage = sportMainPage.selectFirstElementFromThePickedJustForYouSection() ;
        productDetailsPage.selectProductQty(1);
        productDetailsPage.selectProductQty(1);
        productDetailsPage.clickAddToCartButton();
        final String productTitleBeforeAddToCart = productDetailsPage.getProductTitle();
        final String productTitleAfterAddToCart = productDetailsPage.getMiniCartPopupTitle();
//        final Integer actualProductQty = productDetailsPage.getMiniCartPopupQty();
        Assert.assertEquals("Product title is wrong", productTitleBeforeAddToCart, productTitleAfterAddToCart);

    }
}
