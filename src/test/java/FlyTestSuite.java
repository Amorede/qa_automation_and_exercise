import Fly.FlyMainPage;
import Fly.FlyResultPage;
import common.BaseTest;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class FlyTestSuite extends BaseTest {

    @Test
    public void checkEachElementHasCertainData (){
        final String finalDate = "Depart - Wed 2 Jan 2019";
        getDriver().get("https://www.fly.com/");
        final FlyMainPage flyMainPage = new FlyMainPage(getDriver());
        flyMainPage.clickOneWayButton();
        flyMainPage.clickCalendar();
        flyMainPage.clickNextUntillMonthAndDate("January", "2");
        flyMainPage.clickInputFrom();
        flyMainPage.typeFrom("Bost");
        flyMainPage.clickAutoInputFromBOB();
        flyMainPage.clickInputTo();
        flyMainPage.typeTo("Bora");
        flyMainPage.clickAutoInputToJBC();
//        flyMainPage.clickcheapOairCheckbox();
//        flyMainPage.clickJetCostCheckbox();
        final FlyResultPage flyResultPage = flyMainPage.clickSearchBtn();
        flyResultPage.clickOnEveryButton();
        final List<String> actualTitles = flyResultPage.getFilterDates();
        for (String item : actualTitles) {
            Assert.assertTrue("There is no required item in the search result list!", item.contains(finalDate));
        }
    }
}
