import Fly.FlyMainPage;
import Hotline.HotlineAutoPage;
import Hotline.HotlineMainPage;
import common.BaseTest;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class HotlineTest extends BaseTest {

    @Test
    public void hotlineTestCategoriesTitle() {
        final String[] expectedFilterItems = {
                "Зимова гума",
                "Автозапчастини",
                "Автомобільні акумулятори",
                "Антифризи",
                "Автомагнітоли",
                "Відеореєстратори",
        };
        getDriver().get("https://hotline.ua/");
        final HotlineMainPage hotlineMainPage = new HotlineMainPage(getDriver());
        final HotlineAutoPage hotlineAutoPage = hotlineMainPage.clickAutoBtn();
        final List<String> actualTitles = hotlineAutoPage.getTitlesOfTheCategoriesOnTheAutoPage();
        Assert.assertArrayEquals("Wrong Titles",
                expectedFilterItems, actualTitles.toArray());
    }
}