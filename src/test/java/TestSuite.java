import Core.GoogleMainPage;
import Core.SearchResultPage;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import java.util.concurrent.TimeUnit;


public class TestSuite {

    private WebDriver driver;

    @Before
    public void setDriver(){
        System.setProperty("webdriver.firefox.driver", "gecodriver.exe");
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    @After
    public void driverClose (){
        driver.close();
    }


    @Test
    public void checkGoogleMainPageIsOpened (){
        final String expectedTitle = "Google";
        driver.get("https://google.com");
        final String actualTitle = driver.getTitle();
        Assert.assertEquals("There is incorrect page title!", expectedTitle, actualTitle);
    }

    @Test
    public void checkGoogleSearchFunctionality() {
        final String expectedFirstLinkText = "Компьютерная школа Hillel: курсы IT технологий";
        driver.get("https://google.com");
        final GoogleMainPage page = new GoogleMainPage(driver);
        final SearchResultPage searchResultPage = page.typeSearchText("Hillel");
//         = page.clickSearchButton() ;
        final String actualFirstLinkText = searchResultPage.getFirstSearchResultLinkText();
        Assert.assertEquals("there is incorrect first link",
                expectedFirstLinkText, actualFirstLinkText);
    }
}

