import Boocking.BoockingResultPage;
import Boocking.BookingMainPage;
import common.BaseTest;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class BockingTestSuite extends BaseTest {

    @Test
    public void checkBookingDestination() {
        String city = "Boston";
        getDriver().get("https://www.booking.com/index.en-gb.html");
        final BookingMainPage bookingMainPage = new BookingMainPage(getDriver());
        bookingMainPage.clickOnClosePopUp();
        bookingMainPage.clickOnGuests();
        bookingMainPage.setAddSelectAdults(5, "5");
        bookingMainPage.clickOnCalendar();
        bookingMainPage.selectMonth("January 2019");
        bookingMainPage.selectStartDay("11");
        bookingMainPage.selectEndDay("25");
        bookingMainPage.typeWhereToGo("bo");
        bookingMainPage.clickOnBoston();
        final BoockingResultPage boockingResultPage = bookingMainPage.clickSearchButton();
        final List<String> actual = boockingResultPage.getListOfDestination();
        for (String item : actual) {
            Assert.assertTrue("There is no required item in the search result list!", item.contains(city));
        }
    }
}