import common.BaseTest;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.WebElement;
import rozetka.RozetkaMainPage;
import rozetka.RozetkaResultPage;
import rozetka.RozetkaWishlist;

public class RozetkaTest extends BaseTest {

    @Test
    public void rozetkaTest (){
        getDriver().get("https://rozetka.com.ua/");
        final RozetkaMainPage rozetkaMainPage = new RozetkaMainPage(getDriver());
        final RozetkaResultPage rozetkaResultPage = rozetkaMainPage.inputProductName("apple iphone xs max");
        final WebElement actualPhone = rozetkaResultPage.getActualPhone();
        rozetkaResultPage.clickAddToWishList();
        rozetkaResultPage.inputFakeEmail("satanпаап611166xzx@gmail.com");
        rozetkaResultPage.clickSaveFakeEmail();
        rozetkaResultPage.inputFakeName("Тарас Шевченко");
        rozetkaResultPage.clickSaveName();
        final RozetkaWishlist rozetkaWishlist = rozetkaResultPage.clickOnWishlist();
        final WebElement expextedPhone = rozetkaWishlist.getIphoneAtWishlist();
        Assert.assertEquals("Is Not In Wishlist", actualPhone, expextedPhone);
    }
}
